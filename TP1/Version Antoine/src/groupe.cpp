﻿#include <string>
#include "groupe.h"
using namespace std;

//Constructeurs
Groupe::Groupe() {
	nom_ = "inconnu";
	totalDepenses_ = 0;
	nombreDepenses_ = 0;
	nombreUtilisateurs_ = 0;
	tailleTabUtilisateurs_ = 5;
	tailleTabDepenses_ = 10;
	listeUtilisateurs_ = new Utilisateur*[tailleTabUtilisateurs_];
	listeDepenses_ = new Depense*[tailleTabDepenses_];
	comptes_ = nullptr;
	listeTransferts_ = nullptr;
	nombreTransferts_ = 0;
}

Groupe::Groupe(string& nom, unsigned int tailleTabDepenses, unsigned int tailleTabUtilisateurs) {
	nom_ = nom;
	totalDepenses_ = 0;
	// listeUtilisateurs_ = new Utilisateur*(tailleTabUtilisateurs_);
	nombreDepenses_ = 0;
	nombreUtilisateurs_ = 0;
	tailleTabUtilisateurs_ = tailleTabUtilisateurs;
	tailleTabDepenses_ = tailleTabDepenses;
	// listeDepenses_ = nullptr;
	comptes_ = nullptr;
	listeTransferts_ = nullptr;
	nombreTransferts_ = 0;
	listeUtilisateurs_ = new Utilisateur*[tailleTabUtilisateurs];
	listeDepenses_ = new Depense*[tailleTabDepenses];
	
}

//Destructeur
Groupe::~Groupe() {

	delete[] listeUtilisateurs_;
	listeUtilisateurs_ = nullptr;

	delete comptes_;
	comptes_ = nullptr;

	delete[] listeDepenses_;
	listeDepenses_ = nullptr;

	for (unsigned int i = 0; i < nombreTransferts_; i++) {
		delete listeTransferts_[i];
		listeTransferts_[i] = nullptr;
	}

	delete[] listeTransferts_;
	listeTransferts_ = nullptr;

}

//methode d'acces
string Groupe::getNom() const{
	return nom_;
}

unsigned int Groupe::getNombreDepenses() const{
	return nombreDepenses_;
}

double Groupe::getTotal() const{
	return totalDepenses_;
}

//Methodes de modification
void Groupe::setNom(string& nom) {
	nom_ = nom;
}

//Methodes d'ajout
void Groupe::ajouterDepense(Depense* uneDepense, Utilisateur* payePar) {
	
	if (tailleTabDepenses_ <= nombreDepenses_) {
		Depense** temporaire = new Depense*[tailleTabDepenses_];
		for(unsigned int i = 0; i < nombreDepenses_ ; i++) 
		{
			temporaire[i] = listeDepenses_[i];
		}
		unsigned int nouvelleCapacite = tailleTabDepenses_;
		nouvelleCapacite = (tailleTabDepenses_ > 0) ? tailleTabDepenses_ * 2 : 10;
		listeDepenses_ = new Depense*[nouvelleCapacite];
		for(unsigned int i = 0; i < nombreDepenses_; i++)
		{
		listeDepenses_[i] = temporaire[i];
		}
	tailleTabDepenses_ = nouvelleCapacite;
	}
	nombreDepenses_++;
	listeDepenses_[nombreDepenses_-1] = uneDepense;
	//delete[] temporaire;

	payePar->ajouterDepense(uneDepense);

}


void Groupe::ajouterUtilisateur(Utilisateur* unUtilisateur) {

	if (tailleTabUtilisateurs_ <= nombreUtilisateurs_) {
		Utilisateur** temporaire = new Utilisateur*[tailleTabUtilisateurs_];
		for(unsigned int i = 0; i < nombreUtilisateurs_ ; i++) 
		{
			temporaire[i] = listeUtilisateurs_[i];
		}
		unsigned int nouvelleCapacite = tailleTabUtilisateurs_;
		nouvelleCapacite = (tailleTabUtilisateurs_ > 0) ? tailleTabUtilisateurs_ * 2 : 10;
		listeUtilisateurs_ = new Utilisateur*[nouvelleCapacite];
		for(unsigned int i = 0; i < nombreUtilisateurs_; i++)
		{
		listeUtilisateurs_[i] = temporaire[i];
		}
	tailleTabUtilisateurs_ = nouvelleCapacite;
	}
	nombreUtilisateurs_++;
	listeUtilisateurs_[nombreUtilisateurs_-1] = unUtilisateur;

	//delete[] temporaire;
}

//Méthode de calcul
void Groupe::calculerTotalDepenses()
{
	totalDepenses_ = 0;

	for (unsigned int i = 0; i < nombreDepenses_; i++) 
		totalDepenses_ += listeDepenses_[i]->getMontant();

	unsigned int moyenneDepenses = totalDepenses_ / nombreUtilisateurs_;

	for (unsigned int i = 0; i < nombreUtilisateurs_; i++){
		listeUtilisateurs_[i]->calculerTotal();
		comptes_[i] =  moyenneDepenses - listeUtilisateurs_[i]->getTotal();
	}
}

void Groupe::equilibrerComptes()
{
	nombreTransferts_ = nombreUtilisateurs_ - 1;
	listeTransferts_ = new Transfert*[nombreTransferts_];
	for(unsigned int i = 0; i < nombreTransferts_; i++){
		listeTransferts_[i]->setDonneur(listeUtilisateurs_[i+1]);
		listeTransferts_[i]->setReceveur(listeUtilisateurs_[i]);
		listeTransferts_[i]->setMontant(comptes_[i]);
	}
	for (unsigned int i = 0; i < nombreUtilisateurs_; i++){
		comptes_[i] = 0;
	}
}

//methode d'affichage
void Groupe::afficherGroupe() const
{
	cout << "L'évènement : " << nom_ << " a couté un total de : " << totalDepenses_ << "$." << endl << endl;
	cout << "Liste des utilisateurs et leurs dépenses : " << endl << endl;
	for(unsigned int i = 0; i < nombreUtilisateurs_; i++){
		listeUtilisateurs_[i]->afficherUtilisateur();
	}
	cout << "Afin d'équilibrer les comptes, les transferts suivants ont été réalisés : " << endl;
	for(unsigned int i = 0; i < nombreUtilisateurs_; i++){
		listeTransferts_[i]->afficherTransfert();
	}
	cout << "Les comptes sont les suivants : " << endl;
	for(unsigned int i = 0; i < nombreUtilisateurs_; i++){
		cout << listeUtilisateurs_[i]->getNom() << " : " << comptes_[i] << endl;
	}
}
