#include <iostream>
#include "utilisateur.h"
#include <string>
using namespace std;

//Constructeurs
Utilisateur::Utilisateur() {
	nom_ = "inconnu";
	tailleTabDepense_ = 5;
	nombreDepenses_ = 0;
	totalDepense_ = 0;
	listeDepenses_ = nullptr;
}

Utilisateur::Utilisateur(string& nom) {
	nom_ = nom;
	tailleTabDepense_ = 5;
	nombreDepenses_ = 0;
	totalDepense_ = 0;
	listeDepenses_ = new Depense*[tailleTabDepense_];
}

//Destructeur
Utilisateur::~Utilisateur() {
	for (unsigned int i = 0; i < nombreDepenses_; i++) {
		delete listeDepenses_[i];
		listeDepenses_[i] = nullptr;
	}
	delete[] listeDepenses_;
	listeDepenses_ = nullptr;
}

//Methode d'acces
string Utilisateur::getNom() const {
	return nom_;
}

unsigned int Utilisateur::getNombreDepense() const {
	return nombreDepenses_;
}

double Utilisateur::getTotal()  const {
	return totalDepense_;
}

//Methode de modification
void Utilisateur::setNom(string& nom) {
	nom_ = nom;
}

// Methode ajout de depense
void Utilisateur::ajouterDepense(Depense* uneDepense) {

	if (tailleTabDepense_ <= nombreDepenses_) {

		Depense** temporaire = new Depense*[tailleTabDepense_];
		for(unsigned int i = 0; i < nombreDepenses_ ; i++) 		
			temporaire[i] = listeDepenses_[i];
		
		unsigned int nouvelleCapacite = tailleTabDepense_;
		nouvelleCapacite = (tailleTabDepense_ > 0) ? tailleTabDepense_ * 2 : 10;
		listeDepenses_ = new Depense*[nouvelleCapacite];
		for(unsigned int i = 0; i < nombreDepenses_; i++)
			listeDepenses_[i] = temporaire[i];
		
		tailleTabDepense_ = nouvelleCapacite;

		}
	nombreDepenses_++;
	listeDepenses_[nombreDepenses_-1] = uneDepense;
}

//Methode de calcul total
void Utilisateur::calculerTotal() {
	totalDepense_ = 0;
	for (unsigned int i = 0; i < nombreDepenses_; i++) {
		totalDepense_ += listeDepenses_[i]->getMontant();
	}
}

//Mathode d'affichage
void Utilisateur::afficherUtilisateur() const {
	cout << "Utilisateur : " << getNom() << endl << endl;
	cout << "Il a depense un total de " << getTotal() << "$." << endl << endl;
	for(unsigned int i = 0; i <= nombreDepenses_ ; i++){
		cout << "Achat : " << listeDepenses_[i]->getTitre() << endl;
		cout << "Prix : " << listeDepenses_[i]->getMontant() << endl << endl;
	}
	
}
