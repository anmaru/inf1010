#include <iostream>
using namespace std;

#include "transfert.h"

Transfert::Transfert()
{
	montant_ = 0;
	donneur_ = nullptr;
	receveur_ = nullptr;
}

/* 
Transfert::Transfert(double montant)
{
	montant_ = montant;
	donneur_ nullptr;
	receveur_ = nullptr;
}

Transfert::Transfert(double montant, Utilisateur* de)
{
	montant_ = montant;
	donneur_ = de;
}
 */
Transfert::Transfert(double montant, Utilisateur* de, Utilisateur* pour)
{
	montant_ = montant;
	donneur_ = de;
	receveur_ = pour;
}


Utilisateur* Transfert::getDonneur() const
{
	return donneur_;
}

Utilisateur* Transfert::getReceveur() const
{
	return receveur_;
}

double Transfert::getMontant() const
{
	return montant_;
}

void Transfert::setDonneur(Utilisateur* donneur)
{
	donneur_ = donneur;
}

void Transfert::setReceveur(Utilisateur* receveur)
{
	receveur_ = receveur;
}

void Transfert::setMontant(double montant)
{
	montant_ = montant;
}

void Transfert::afficherTransfert() const
{	
	if(montant_ < 0)
		cout << receveur_ << " a donné " << abs(montant_) << "$ à " << donneur_ << endl;
	else
		cout << donneur_ << " a donné " << montant_ << "$ à " << receveur_ << endl;
	
}


