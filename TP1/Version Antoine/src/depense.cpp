#include <iostream>
#include <string>
#include "depense.h"
using namespace std;

Depense::Depense()
{
	titre_ = "aucune dépense";
	montant_ = 0;
}
/* 
Depense::Depense(string& titre)
{
	titre_ = titre;
	montant_ = 0;
}
 */
Depense::Depense(string& titre, double montant)
{
	titre_ = titre;
	montant_ = montant;
}

string Depense::getTitre() const
{
	return titre_;
}


void Depense::setTitre(string& titre)
{
	titre_ = titre;
}

double Depense::getMontant() const 
{
	return montant_;
}

void Depense::setMontant(double montant)
{
	montant_ = montant;
}

void Depense::afficherDepense() const 
{
	cout << "" << Depense::getMontant() << "$ a ete depense sur" << Depense::getTitre() << endl;
	
}