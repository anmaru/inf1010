#include <stdlib.h>

#ifdef _WIN32
#define WINPAUSE system("pause")
#endif

#include <iostream>
#include "groupe.h"

using namespace std;
 
//david.dratwa@polymtl.ca pour questions
int main() {
    cout << "\t \tBienvenue sur PolyCount " << endl << " *****************************************" << endl;
    // Creer un  groupe pour  6 depenses et 4 utilisateurs.

    string noms[5] = {"Joséphine", "Marie-Bobbette", "Tony G", "SwiftMove", "OG_SKATER_34"};
    string depenses[7] = {"Chips", "Liqueur", "Coke", "Plus de liqueur", "Sweet Munchies", "Dope decoration", "Cake"};
    string nomDuGroupe = "Party à Joséphine";
   
    Groupe* ungroupe = new Groupe(nomDuGroupe, 5, 7);
    // Creer 5 utlisateurs.

    Utilisateur* u1 = new Utilisateur(noms[0]);
    Utilisateur* u2 = new Utilisateur(noms[1]);
    Utilisateur* u3 = new Utilisateur(noms[2]);
    Utilisateur* u4 = new Utilisateur(noms[3]);
    Utilisateur* u5 = new Utilisateur(noms[4]);
    
    // Creer 7 dépenses.
    
    Depense* d1 = new Depense(depenses[0], 30);
    Depense* d2 = new Depense(depenses[1], 10);
    Depense* d3 = new Depense(depenses[2], 15);
    Depense* d4 = new Depense(depenses[3], 120);
    Depense* d5 = new Depense(depenses[4], 85);
    Depense* d6 = new Depense(depenses[5], 70);
    Depense* d7 = new Depense(depenses[6], 50);
    
    //ajouter les utilisateurs au groupe
  
    ungroupe->ajouterUtilisateur(u1);
    ungroupe->ajouterUtilisateur(u2);
    ungroupe->ajouterUtilisateur(u3);
    ungroupe->ajouterUtilisateur(u4);
    ungroupe->ajouterUtilisateur(u5);

    // ajouter les depenses au groupe
    
    ungroupe->ajouterDepense(d1, u1);
    ungroupe->ajouterDepense(d2, u1);
    ungroupe->ajouterDepense(d3, u2);
    ungroupe->ajouterDepense(d4, u2);
    ungroupe->ajouterDepense(d5, u3);
    ungroupe->ajouterDepense(d6, u4);
    ungroupe->ajouterDepense(d7, u5);
    
    // calculer le total du grouoe et de chaque utilisateur

    ungroupe->calculerTotalDepenses();

    // Afficher  le groupe

    ungroupe->afficherGroupe();

    //Equilibrer les comptes

    ungroupe->equilibrerComptes();

    //Afficher le groupe

    ungroupe->afficherGroupe();

    //terminer le programme correctement
    
    return 0;
}
