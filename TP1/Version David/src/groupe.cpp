﻿
#include <string>
#include "groupe.h"
using namespace std;

//Constructeurs

Groupe::Groupe() {
	nom_ = "inconnu";
	totalDepenses_ = 0;
	nombreDepenses_ = 0;
	nombreUtilisateurs_ = 0;
	tailleTabUtilisateurs_ = 5;
	tailleTabDepenses_ = 10;
	listeUtilisateurs_ = nullptr;
	listeDepenses_ = nullptr;
	comptes_ = nullptr;
	listeTransferts_ = nullptr;
	nombreTransferts_ = 0;
}

Groupe::Groupe(string& nom, unsigned int tailleTabDepenses, unsigned int tailleTabUtilisateurs) {
	nom_ = nom;
	totalDepenses_ = 0;
	nombreDepenses_ = 0;
	nombreUtilisateurs_ = 0;
	tailleTabUtilisateurs_ = tailleTabUtilisateurs;
	tailleTabDepenses_ = tailleTabDepenses;
	comptes_ = nullptr;
	listeTransferts_ = nullptr;
	nombreTransferts_ = 0;
	listeUtilisateurs_ = new Utilisateur*[tailleTabUtilisateurs];
	listeDepenses_ = new Depense*[tailleTabDepenses];
}

//Destructeur
Groupe::~Groupe() {

	for (unsigned int i = 0; i < nombreUtilisateurs_; i++) {
		delete listeUtilisateurs_[i];
		listeUtilisateurs_[i] = nullptr;
	}
	delete[] listeUtilisateurs_;
	listeUtilisateurs_ = nullptr;

	delete comptes_;
	comptes_ = nullptr;

	delete[] listeDepenses_;
	listeDepenses_ = nullptr;

	for (unsigned int i = 0; i < nombreTransferts_; i++) {
		delete listeTransferts_[i];
		listeTransferts_[i] = nullptr;
	}

	delete[] listeTransferts_;
	listeTransferts_ = nullptr;

}

//methode d'acces
string Groupe::getNom() const {
	return nom_;
}

unsigned int Groupe::getNombreDepenses() const {
	return nombreDepenses_;
}

double Groupe::getTotal() const {
	return totalDepenses_;
}

//Methodes de modification
void Groupe::setNom(string& nom) {
	nom_ = nom;
}

//Methodes d'ajout
void Groupe::ajouterDepense(Depense* uneDepense, Utilisateur* payePar) {

	if (tailleTabDepenses_ <= nombreDepenses_) {

		unsigned int nouvelleCapacite = (tailleTabDepenses_ > 0) ? tailleTabDepenses_ * 2 : 10;

		Depense** temporaire = new Depense*[nouvelleCapacite];

		for (unsigned int i = 0; i < nombreDepenses_; i++)
		{
			temporaire[i] = listeDepenses_[i];
		}

		listeDepenses_ = temporaire;
		tailleTabDepenses_ = nouvelleCapacite;

		}
	nombreDepenses_++;
	listeDepenses_[nombreDepenses_ - 1] = uneDepense;
	payePar->ajouterDepense(uneDepense);

}


void Groupe::ajouterUtilisateur(Utilisateur* unUtilisateur) {

	if (tailleTabUtilisateurs_ <= nombreUtilisateurs_) {

		unsigned int nouvelleCapacite = (tailleTabUtilisateurs_ > 0) ? tailleTabUtilisateurs_ * 2 : 10;

		Utilisateur** temporaire = new Utilisateur*[nouvelleCapacite];
		for (unsigned int i = 0; i < nombreUtilisateurs_; i++)
		{
			temporaire[i] = listeUtilisateurs_[i];
		}

		tailleTabUtilisateurs_ = nouvelleCapacite;
		listeUtilisateurs_ = temporaire;
	}
	nombreUtilisateurs_++;
	listeUtilisateurs_[nombreUtilisateurs_ - 1] = unUtilisateur;
}

//Méthode de calcul
void Groupe::calculerTotalDepenses()
{
	totalDepenses_ = 0;

	for (unsigned int i = 0; i < nombreDepenses_; i++) 
		totalDepenses_ += listeDepenses_[i]->getMontant();
	

	double moyenneDepenses = totalDepenses_ / nombreUtilisateurs_;

	comptes_ = new double[nombreUtilisateurs_];

	for (unsigned int i = 0; i < nombreUtilisateurs_; i++) {
		listeUtilisateurs_[i]->calculerTotal();
		comptes_[i] = moyenneDepenses - listeUtilisateurs_[i]->getTotal();
	}
}


void Groupe::equilibrerComptes()
{
	nombreTransferts_ = nombreUtilisateurs_ - 1;
	listeTransferts_ = new Transfert*[nombreTransferts_];

	for (unsigned int i = 0; i < nombreTransferts_; i++) {

		double montant = comptes_[i];
		listeTransferts_[i] = new Transfert(montant, listeUtilisateurs_[i + 1], listeUtilisateurs_[i]);
		comptes_[i] -= montant;
		comptes_[i + 1] += montant;

	}	
}

//methode d'affichage
void Groupe::afficherGroupe() const
{
	cout << "L'évènement : " << nom_ << " a coûté un total de : " << totalDepenses_ << "$." << endl << endl;
	cout << "Liste des utilisateurs et leurs dépenses : " << endl << endl;
	for (unsigned int i = 0; i < nombreUtilisateurs_; i++) {
		listeUtilisateurs_[i]->afficherUtilisateur();
	}
	if(!(listeTransferts_ == nullptr)) {
		cout << "Afin d'équilibrer les comptes, les transferts suivants ont été réalisés : " << endl;
	for (unsigned int i = 0; i < nombreTransferts_; i++) 
		listeTransferts_[i]->afficherTransfert();
	}
	cout << "Les comptes sont les suivants : " << endl;
	for (unsigned int i = 0; i < nombreUtilisateurs_; i++) {
		cout << listeUtilisateurs_[i]->getNom() << " : " << comptes_[i] << endl;
	}
	
}
